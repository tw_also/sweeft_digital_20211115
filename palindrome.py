# 1. დაწერეთ ფუნქცია, რომელსაც გადაეცემა ტექსტი  და აბრუნებს პალინდრომია თუ არა.
# (პალინდრომი არის ტექსტი რომელიც ერთნაირად იკითხება ორივე მხრიდან). Boolean isPalindrome(String text);


def is_palindrome(text):
    return text == text[::-1]


if __name__ == "__main__":
    simple_input = [
        "1221",
        "abcdeffedcba",
        "1234",
        "non-palindrome-text",
    ]

    for text in simple_input:
        if is_palindrome(text):
            print(f"yes: {text}")
        else:
            print(f"no: {text}")
