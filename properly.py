# 4. მოცემულია String რომელიც შედგება „(„ და „)“ ელემენტებისგან. დაწერეთ ფუნქცია რომელიც აბრუნებს ფრჩხილები არის თუ არა
# მათემატიკურად სწორად დასმული: Boolean isProperly(String sequence); მაგ: (()()) სწორი მიმდევრობაა, ())() არასწორია


def is_properly(sequence):
    counter = 0
    for i in sequence:
        if i == "(":
            counter += 1
        elif i == ")":
            counter -= 1
        if counter < 0:
            return False
    return counter == 0


if __name__ == "__main__":
    print(is_properly("(()())"))  # True
    print(is_properly("())()"))   # False
    print(is_properly(")))((("))  # False
    print(is_properly("((()"))    # False
