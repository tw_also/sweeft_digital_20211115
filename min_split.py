# 2. გვაქვს 1, 5, 10, 20 და 50 თეთრიანი მონეტები. დაწერეთ ფუნქცია, რომელსაც გადაეცემა თანხა (თეთრებში) და
# აბრუნებს მონეტების მინიმალურ რაოდენობას, რომლითაც შეგვიძლია ეს თანხა დავახურდაოთ. Int minSplit(Int amount);


def min_split(amount):
    total_coins = 0

    for i in [50, 20, 10, 5, 1]:
        coins = amount // i
        total_coins += coins
        amount -= coins * i

    return total_coins


if __name__ == "__main__":
    print("0 =", min_split(0), "coins")
    print("1 =", min_split(1), "coins")
    print("2 =", min_split(2), "coins")
    print("5 =", min_split(5), "coins")
    print("6 =", min_split(6), "coins")
    print("10 =", min_split(10), "coins")
    print("15 =", min_split(15), "coins")
    print("16 =", min_split(16), "coins")
    print("20 =", min_split(20), "coins")
    print("30 =", min_split(30), "coins")
    print("35 =", min_split(35), "coins")
    print("36 =", min_split(36), "coins")
    print("40 =", min_split(40), "coins")
    print("50 =", min_split(50), "coins")
    print("70 =", min_split(70), "coins")
    print("80 =", min_split(80), "coins")
    print("85 =", min_split(85), "coins")
    print("86 =", min_split(86), "coins")
    print("100 =", min_split(100), "coins")
    print("247 =", min_split(247), "coins")
