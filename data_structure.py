# 6. დაწერეთ საკუთარი მონაცემთა სტრუქტურა, რომელიც საშუალებას მოგვცემს O(1) დროში წავშალოთ ელემენტი

import json


class DataObject:
    def __init__(self):
        self.data = {}

    def _get_key(self, element):
        return hash(json.dumps(element))

    def add_element(self, element):
        key = self._get_key(element)
        self.data[key] = element

    def remove_element(self, element):
        key = self._get_key(element)
        del self.data[key]

    def get_elements(self):
        return self.data


if __name__ == "__main__":
    my_obj = DataObject()

    my_obj.add_element("Value 1")
    my_obj.add_element("Value 2")
    my_obj.add_element(247)
    my_obj.add_element({"key": "value"})
    my_obj.add_element({"key1": "value2", "key2": "value2", "key3": "value3", "key4": "value4"})

    my_obj.remove_element("Value 2")
    my_obj.remove_element({"key": "value"})

    print(my_obj.get_elements())
