# 3. მოცემულია მასივი, რომელიც შედგება მთელი რიცხვებისგან. დაწერეთ ფუნქცია რომელსაც გადაეცემა ეს მასივი და აბრუნებს
# მინიმალურ მთელ რიცხვს, რომელიც 0-ზე მეტია და ამ მასივში არ შედის. Int notContains(Int[] array);


def not_contains(array):
    min_number = 1
    while min_number in array:
        min_number += 1
    else:
        return min_number


def not_contains_2(my_array):
    for i in range(1, max(my_array) + 2):
        if i not in my_array:
            return i


if __name__ == "__main__":
    print(not_contains([0]), ": [0]")
    print(not_contains([0, 1]), ": [0, 1]")
    print(not_contains([1, 2, 4]), ": [1, 2, 4]")
    print(not_contains([1, 2, 3, 4, 5, 6, 8]), ": [1, 2, 3, 4, 5, 6, 8]")
